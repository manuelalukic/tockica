package com.example.tockicaprojekt;

import java.time.Duration;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

public class CetvrtakGoodVibesTest {
    private WebDriver driver;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/driver/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");

        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void tockicagoodvibescet() {

        driver.get("https://www.tockicanai.hr/raspored-iduci-tjedan/");
        driver.manage().window().setSize(new Dimension(1536, 812));

        //pon dunija #mec_skin_events_62484 > div > div:nth-child(1) > article:nth-child(3) > div > div.tni-tt-booking-wrapper > a
        //cet mario ##mec_skin_events_62484 > div > div:nth-child(4) > article:nth-child(3) > div > div.tni-tt-booking-wrapper > a
        //cet dunija #mec_skin_events_62484 > div > div:nth-child(4) > article:nth-child(2) > div > div.tni-tt-booking-wrapper > a
        //sri ivana #mec_skin_events_62484 > div > div:nth-child(3) > article:nth-child(3) > div > div.tni-tt-booking-wrapper > a
        //uto ivana #mec_skin_events_62484 > div > div:nth-child(2) > article:nth-child(2) > div > div.tni-tt-booking-wrapper > a
        driver.findElement(By.cssSelector("#mec_skin_events_62484 > div > div:nth-child(4) > article:nth-child(2) > div > div.tni-tt-booking-wrapper > a")).click();
        {
            WebElement element = driver.findElement(By.tagName("body"));
            Actions builder = new Actions(driver);
            builder.moveToElement(element, 0, 0).perform();
        }
        driver.switchTo().frame(0);
        driver.findElement(By.id("mec-book-form-btn-step-1")).click();
        driver.findElement(By.id("mec_book_reg_field_name1")).click();
        driver.findElement(By.id("mec_book_reg_field_name1")).sendKeys("Manuela Lukić");
        driver.findElement(By.id("mec_book_reg_field_email0")).sendKeys("manuela.lukic@gmail.com");
        driver.findElement(By.id("mec_book_reg_field_reg1_2")).sendKeys("0989174246");
        driver.findElement(By.id("mec_book_reg_field_reg1_3")).click();
        driver.findElement(By.id("mec-book-form-btn-step-2")).click();
    }
}
